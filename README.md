# About
WindWidget2011 is a Qt widget that performs calculations of design wind speed according to Australian standards. This particular widget follows the process as defined in AS/NZS 1170.2:2011, and will not be applicable to other standards.

The end product will be a widget that can be implemented in a Qt-based application suite.

# Features
Currently there is only basic functionality, with most initial focus on developing the UI:
- Selection of wind region, terrain, and AEP classes.
- Calculations based on structure height.
- Manual shielding input.

# Building
To be completed. You will most likely need the Qt framework installed, and something like Qt Creator to compile it. Windows/macOS have not currently been tested, YMMV.

# TODO
Most of the functionality is not yet implemented:
- Slope calculation
- All cardinal directions
- Shielding calculation
- Lee factors
