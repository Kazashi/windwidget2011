#include "windwidget2011.h"

// Generate values for QMaps and matrices
void WindWidget2011::buildMaps()
{
    // Values for AEP%, determined from tables in AS 1170.0
    AEPMap.insert("V1", 1);
    AEPMap.insert("V5", 5);
    AEPMap.insert("V10", 10);
    AEPMap.insert("V20", 20);
    AEPMap.insert("V25", 25);
    AEPMap.insert("V50", 50);
    AEPMap.insert("V100", 100);
    AEPMap.insert("V200", 200);
    AEPMap.insert("V250", 250);
    AEPMap.insert("V500", 500);
    AEPMap.insert("V1000", 1000);
    AEPMap.insert("V2000", 2000);
    AEPMap.insert("V2500", 2500);
    AEPMap.insert("V5000", 5000);
    AEPMap.insert("V10000", 10000);

    // A1-A5, B, C, D = Aus Regions. A6, A7, W = NZ Regions
    RegMap.insert(1, "A1");
    RegMap.insert(2, "A2");
    RegMap.insert(3, "A3");
    RegMap.insert(4, "A4");
    RegMap.insert(5, "A5");
    RegMap.insert(6, "A6");
    RegMap.insert(7, "A7");
    RegMap.insert(8, "W");
    RegMap.insert(9, "B");
    RegMap.insert(10, "C");
    RegMap.insert(11, "D");

    // terrain categories, mapped out for use in matrix operations
    MzMap.insert("TC1", 0);
    MzMap.insert("TC1.5", 1);
    MzMap.insert("TC2", 2);
    MzMap.insert("TC2.5", 3);
    MzMap.insert("TC3", 4);
    MzMap.insert("TC4", 5);

    // Matrix values for wind direction multipliers (row = cardinal, column = region)
    float windDirectionValues[] = {
        0.90f, 0.80f, 0.85f, 0.90f, 1.00f, 0.85f, 0.9f, 1.0f,
        0.80f, 0.80f, 0.80f, 0.85f, 0.85f, 0.95f, 0.90f, 0.95f,
        0.80f, 0.80f, 0.80f, 0.90f, 0.80f, 1.00f, 0.80f, 0.80f,
        0.80f, 0.95f, 0.80f, 0.90f, 0.80f, 0.95f, 0.90f, 0.90f,
        0.85f, 0.90f, 0.80f, 0.95f, 0.85f, 0.85f, 0.90f, 1.00f,
        0.95f, 0.95f, 0.85f, 0.95f, 0.90f, 0.95f, 0.90f, 1.00f,
        1.00f, 1.00f, 0.90f, 0.95f, 1.00f, 1.00f, 1.00f, 0.90f,
        0.95f, 0.95f, 1.00f, 0.90f, 0.95f, 0.95f, 1.00f, 0.95f
    };

    // currently values up to 15m (TC 1.5 and 2.5 included)
    // Note: maximum value of QDoubleSpinBox heightSelect in mainwindow.ui must be changed with
    // extra heights added
    float heightMultiplierValues[] = {
        0.99f, 0.95f, 0.91f, 0.87f, 0.83f, 0.75f,
        1.05f, 0.98f, 0.91f, 0.87f, 0.83f, 0.75f,
        1.12f, 1.06f, 1.00f, 0.915f, 0.83f, 0.75f,
        1.16f, 1.105f, 1.05f, 0.97f, 0.89f, 0.75f
    };

    MdMatrix = new QGenericMatrix<8,8,float>(windDirectionValues);
    MzMatrix = new QGenericMatrix<6,3,float>(heightMultiplierValues);
}
