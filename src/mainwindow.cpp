#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setMinimumSize(640,480);

    windWidget = new WindWidget2011;
    setCentralWidget(windWidget);
    setupMenu();

}

MainWindow::~MainWindow()
{
    delete windWidget;
}

void MainWindow::setupMenu()
{
    fileMenu = menuBar()->addMenu("File");
    helpMenu = menuBar()->addMenu("Help");

    quitAction = new QAction("Quit");
    connect(quitAction, SIGNAL(triggered(bool)), this, SLOT(close()));
    fileMenu->addAction(quitAction);
}
