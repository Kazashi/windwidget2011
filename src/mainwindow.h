#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <QMenu>

#include "windwidget2011.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    WindWidget2011 *windWidget;

private:
    void setupMenu();

    QMenu *fileMenu;
    QMenu *helpMenu;

    QAction *quitAction;
};

#endif // MAINWINDOW_H
