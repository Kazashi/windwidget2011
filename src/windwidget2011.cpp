#include "windwidget2011.h"
#include "ui_windwidget2011.h"

WindWidget2011::WindWidget2011(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WindWidget2011)
{
    ui->setupUi(this);

    buildMaps();

    regionMapIndex = 1;
    tcMapIndex = 0;

    buildingHeight = 2.0;
    AEPValue = 1;
//    hillValue = 1.0;
    leeValue = 1.0;

    finalValue = 0.0;
    regionWindSpeed = 0.0;
    windDirectionMultiplier = 1.0;
    mzCatValue = 1.0;
    shieldValue = 1.0;
    topoValue = 1.0;
}

WindWidget2011::~WindWidget2011()
{
    delete ui;
}


int WindWidget2011::calculateRegionSpeed()
{
    int windSpeed = 0;

    // values for AEP > 1 are calculated via formula as provided in AS 1170.2
    switch (regionMapIndex)
    {
    case 8: // Region W
        (AEPValue == 1) ? windSpeed = 34 : windSpeed = qRound(104 - (70*qPow(AEPValue, -0.045)));
        break;
    case 9: // Region B
        (AEPValue == 1) ? windSpeed = 26 : windSpeed = qRound(106 - (92*qPow(AEPValue, -0.1)));
        break;
    case 10: // Region C (TBC)
        windSpeed = 100;
        break;
    case 11: // Region D (TBC)
        windSpeed = 200;
        break;
    default: // A-series
        (AEPValue == 1) ? windSpeed = 30 : windSpeed = qRound(67 - (41*qPow(AEPValue, -0.1)));
        break;
    }

    return windSpeed;
}

void WindWidget2011::calculateDirectionMultiplier()
{
    float windArray[8];

    int columNum = regionMapIndex - 1;

    // only applies to A1-A7 and W regions
    if(columNum < 8)
    {
        for (int i = 0;i < 8; i++)
        {
            windArray[i] =  MdMatrix->operator()(i, columNum);
        }
    }
}

// interpolate between values in table to resolve Mz,cat
qreal WindWidget2011::calculateMzcatValue()
{
    // take terrain category, use values from column to fill array
    float mzArray[4];

    int column = tcMapIndex;
    for(int i = 0; i < 4; i++)
    {
        mzArray[i] = MdMatrix->operator()(i, column);
    }

    // take height value, find which m values it lies between (<3, 5, 10 to start with)

    float val1 = 0.0f;
    float val2 = 0.0f;
    float height1 = 0.0f;
    float height2 = 0.0f;

    if(buildingHeight <= 3)
    {
        height1 = 2.0f;
        height2 = 3.0f;
        val1 = 0.0f;
        val2 = mzArray[0];
    }
    else if(buildingHeight <= 5)
    {
        height1 = 3.0f;
        height2 = 5.0f;
        val1 = mzArray[0];
        val2 = mzArray[1];
    }
    else if(buildingHeight <= 10)
    {
        height1 = 5.0f;
        height2 = 10.0f;
        val1 = mzArray[1];
        val2 = mzArray[2];
    }
    else
    {
        height1 = 10.0f;
        height2 = 15.0f;
        val1 = mzArray[2];
        val2 = mzArray[3];
    }

    // do actual interpolation
    return val1 + ((val2 - val1)/(height2 - height1));
}

// The terrain table should contain values for the height of the hill (msl),
// and the distance of the structure to the crest or trough (m). Total length of slope should
// be the sum of opposing cardinal points.
// We will generate the values H, L, and x - Lu may be an approximation via this method.
void WindWidget2011::calculateHillShape()
{
    int distance = 0;
    int structDist = 0;
    int height = 0;
    qreal Lu = 0;
    qreal L1 = 0;
    qreal L2 = 0;

    qreal slope = 0;
    qreal hillValue = 0;

    int hillArray[2][8] = {{0}};

    // fill an array first
    for(int i = 0; i < 2; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            hillArray[i][j] = ui->tableWidget->item(j,i)->text().toInt();
            qDebug() << hillArray[i][j];
        }
    }

    // TODO: calculate Mh value for each cardinal (i.e. place all this in a loop):
    // 1. calculate slope to determine equation used
    // 2. determine distance (H), structure distance (x), half total distance (Lu), length scales
    // 3. select suitable equation and calculate
    // 4. insert final value into array
    structDist = hillArray[0][0];
    distance = hillArray[0][0] + hillArray[0][3];
    height = hillArray[1][0] - hillArray[1][3];
    slope = distance / height;
    Lu = distance / 2;

    L1 = qMax(Lu * 0.36, height * 0.4);

    if(slope < 0.05)
    {
        hillValue = 1.0;
    }
    else if(slope > 0.45 && structDist < height/4) // if in separation zone i.e. x < H/4
    {
        hillValue = 1 + 0.71 * (1 - (qAbs(structDist)/L2));
    }
    else
    {
        // big equation 1+(H/3.5(z+L1))*(1-(abs(x)/L2))
        hillValue = 1 + (height/(3.5 * (buildingHeight + L1))) * (1 - (qAbs(structDist)/L2));
    }

    topoValues[0] = hillValue;
    // store Mh value in array of 8 cardinals
}

// TODO: iterate through all eight direction multipliers, fill final values
// Select largest value from those final values
void WindWidget2011::calculateFinalValue()
{
    regionWindSpeed = calculateRegionSpeed();
    mzCatValue = calculateMzcatValue();
    calculateDirectionMultiplier();
    calculateHillShape();

    finalValue = regionWindSpeed * windDirectionMultiplier * mzCatValue * shieldValue * topoValue;

    ui->designSpeedValue->setText(QString::number(finalValue));
    ui->VRValue->setText(QString::number(regionWindSpeed));
    ui->MDValue->setText(QString::number(windDirectionMultiplier));
    ui->MZValue->setText(QString::number(mzCatValue));
    ui->MSValue->setText(QString::number(shieldValue));
    ui->MTValue->setText(QString::number(topoValue));
}

// SLOTS begin here.

void WindWidget2011::on_aepSelect_activated(const QString &arg1)
{
    AEPValue = AEPMap[arg1];
}

void WindWidget2011::on_regionSelect_activated(int index)
{
    regionMapIndex = index + 1;
    regValue = RegMap[regionMapIndex];
}

void WindWidget2011::on_terrainSelect_activated(const QString &arg1)
{
    tcMapIndex = MzMap[arg1];
}

void WindWidget2011::on_heightSelect_valueChanged(double arg1)
{
    buildingHeight = arg1;
}

void WindWidget2011::on_calculateButton_clicked()
{
    calculateFinalValue();
}


// slope calculation will take value from cardinal distance, adding its opposite cardinal
// value, before dividing by the difference in height between the two points.
void WindWidget2011::on_tableWidget_cellActivated(int row, int column)
{
    int slopeArray[8];

    for(int i = 0; i < ui->tableWidget->columnCount();i++)
    {
        slopeArray[i] = 0;
//        slopeArray[i] = ui->tableWidget->itemAt(i, 0)->;
    }
    ui->tableWidget->itemAt(row, column);
//    ui->NResult->setText();
}
