#ifndef WINDWIDGET2011_H
#define WINDWIDGET2011_H

#include <QWidget>
#include <QGenericMatrix>
#include <QtMath>

namespace Ui {
class WindWidget2011;
}

class WindWidget2011 : public QWidget
{
    Q_OBJECT

public:
    explicit WindWidget2011(QWidget *parent = nullptr);
    ~WindWidget2011();

private slots:
    void on_aepSelect_activated(const QString &arg1);

    void on_regionSelect_activated(int index);

    void on_terrainSelect_activated(const QString &arg1);

    void on_heightSelect_valueChanged(double arg1);

    void on_calculateButton_clicked();

    void on_tableWidget_cellActivated(int row, int column);

private:
    Ui::WindWidget2011 *ui;
    void buildMaps();
    int calculateRegionSpeed();
    void calculateDirectionMultiplier();
    qreal calculateMzcatValue();
    void calculateHillShape();


    void calculateFinalValue();

    QGenericMatrix<8,8,float> *MdMatrix;
    QGenericMatrix<6, 3, float> *MzMatrix;

    QMap<QString, qint16> AEPMap;
    QMap<int, QString> RegMap;
    QMap<QString, int> MzMap;

    int regionMapIndex; // index for RegMap
    int tcMapIndex; // index for MzMap

    QString regValue;

    // values for intermediary calculations
    qreal buildingHeight; // used to calculate Mz,cat
    int AEPValue; // used to calculate Vreg
//    qreal hillValue; // used to calculate Mt
    qreal leeValue; // ditto

    qreal topoValues[8]; // used to store Mh/Mt

// Values used in final equation (Vsite = Vreg*Md*(Mz,cat*Ms*Mt)
    qreal finalValue; // Vsite
    qreal regionWindSpeed; // Vr, grabbed from calculateRegionSpeed
    qreal windDirectionMultiplier; // Md
    qreal mzCatValue; // Mz,cat
    qreal shieldValue; // Ms
    qreal topoValue; // Mt
};

#endif // WINDWIDGET2011_H
